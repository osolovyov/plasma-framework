/*
    SPDX-FileCopyrightText: 2022 Aleix Pol Gonzalez <aleixpol@kde.org>

    SPDX-License-Identifier: LGPL-2.0-or-later
*/

import org.kde.plasma.extras 2.0 as PlasmaExtras

/**
 * @see org.kde.plasma.extras Highlight
 */

PlasmaExtras.Highlight
{
}
